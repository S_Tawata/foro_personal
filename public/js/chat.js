import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'                              //Estamos trayendo las funciones de firebase//
/* Estamos trayendo del firebase un elemento*/ 
import { getDatabase, ref, push, onValue,query, orderByKey} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";
/* Estamos usando las 2 funciones de firebase "getAuth, createUserWithEmailAndPassword" */
import { getAuth, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

 //Aqui utlizamos el firebaseconfig que sirve para identificar cada proyecto//

  const firebaseConfig = {                                                                                               
        apiKey: "AIzaSyCqoaa-yaZrmIxs8Jtj2YxX1gzxqVi-J2c",
        authDomain: "proyectotawata.firebaseapp.com",
        dataBbaseURL:"https://proyectotawata-default-rtdb.firebaseio.com/",
        projectId: "proyectotawata",
        storageBucket: "proyectotawata.appspot.com",
        messagingSenderId: "93948459484",
        appId: "1:93948459484:web:ab8a31ba743b90fd167719",
        measurementId: "G-LEGJFBCKX9"
  };
   var UID;
   var correo;

  //Aqui estamos utilizando la funcion initialize app y guardandola en app. Despues utlizamos la funcion Getdatabase con app y la guardamos en database/

  const app = initializeApp(firebaseConfig);                

  const auth = getAuth();
  
  const database = getDatabase(app);
                                                                  
  //En estas lineas se estan guardando en 3 variables las referencias de id del texto, mensaje y el boton//

  var textoRef = document.getElementById("textochatId");

  var botonRef = document.getElementById("buttonchatId");

  var chatRef = document.getElementById("mensajeId");

  //Aqui estamos utilizando la funcion "onAuthStateChanged" que nos sirve para determinar si el usuario inicio sesion o si el usuario no inicio sesion. Para esto utilizamos un IF//

    onAuthStateChanged(auth, (user) => {
    if (user) {
      
        botonRef.addEventListener("click", cargarinformacion);      // Dentro del condicional si el usuario inicio sesion y hace click la funcion cargar informacion se ejecuta//                                       

        const uid = user.uid;
        UID = uid
        console.log(" Su UID es: " + uid )

        const mail = user.email;
        correo = mail

        console.log("Su correo es:" + correo)
    
    } else {
      
        console.log("inicie sesion para escribir")
        botonRef.addEventListener("click", usurio_no_registrado);   // Dentro del condicional si el usuario no inicio sesion y hace click la funcion usuario no registrado  se ejecuta, esta funcion //
    }
  });


  //Aqui la pagina envia al usuario a registrarse y iniciar sesion//
  function usurio_no_registrado() {
        window.location.href = './gestion_personal.html';                                                                                                                                                   
        alert("No iniciaste sesion. Haz click en ACEPTAR para iniciar sesion");
  }
 
  // En esta funcion se escribe un mensaje y lo envia a la base de datos//

  function cargarinformacion () {                                                                                              

      let informacion = textoRef.value;

      push(ref(database, 'datos/'), {
           data: informacion,
           
      });
  
      textoRef.value = ""
      console.log("carga correcta de informacion");
      location.reload()

  }
  
  const querymensajes = query(ref(database, 'datos/'), orderByKey('data'));
  console.log(querymensajes);

  //En esta funcion se lee el mensaje que esta en la base de datos//

  onValue(querymensajes, (snapshot) => {                    

      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val().data;
        console.log(childData);
        
        chatRef.innerHTML += `
            <p>${correo}: ${childSnapshot.val().data}</p>
        `;
    });
  })

  



