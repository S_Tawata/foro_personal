import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
/* Estamos trayendo del firebase un elemento*/
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
/* Estamos usando las 2 funciones de firebase "getAuth, createUserWithEmailAndPassword" */
import { set, ref, getDatabase } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js"

//Aqui utlizamos el firebaseconfig que sirve para identificar cada proyecto//

const firebaseConfig = {                                                                                             //Aqui utlizamos el firebaseconfig que sirve para identificar cada proyecto//
  apiKey: "AIzaSyCqoaa-yaZrmIxs8Jtj2YxX1gzxqVi-J2c",
  authDomain: "proyectotawata.firebaseapp.com",
  dataBbaseURL: "https://proyectotawata-default-rtdb.firebaseio.com/",
  projectId: "proyectotawata",
  storageBucket: "proyectotawata.appspot.com",
  messagingSenderId: "93948459484",
  appId: "1:93948459484:web:ab8a31ba743b90fd167719",
  measurementId: "G-LEGJFBCKX9"
};

console.log("Ingresarse en la pantalla");

//En estas lineas estamos guardando en diferentes variables las referencias del ID de deiferentes componentes//

let emailiniciar_ref = document.getElementById("exampleInputEmail_1");

let passwordiniciar_ref = document.getElementById("exampleInputPassword_1");

let emailregitrar_ref = document.getElementById("exampleInputEmail_2");

let passwordregistrar_ref = document.getElementById("exampleInputPassword_2");

let Nombreyapelldio_ref = document.getElementById("exampleInputnombre_apellido");

let ubicacion_ref = document.getElementById("exampleInputlugar");

let button_ref = document.getElementById("boton123");

button_ref.addEventListener("click", Crear_usuario);

boton1234.addEventListener("click", Login);

//Aqui estamos utilizando la funcion initialize app y guardandola en app. Despues utlizamos la funcion Getdatabase con app y la guardamos en database/


const app = initializeApp(firebaseConfig);

const database = getDatabase(app);

const auth = getAuth(app);

//Registrarse//
//Esta funcion lo que hace es registrar al usuario ingresndo nombre,apellido, ubicacion, mail y contraseña//

function Crear_usuario() {

  //Utilizamos el metodo createUserWithEmailAndPassword este crea el usuario. Este metodo devuelve una promesa//    

  createUserWithEmailAndPassword(auth, emailregitrar_ref.value, passwordregistrar_ref.value)

    //Esta promesa se activa si el usuario se registro correctamente//

    .then((userCredential) => {
      console.log("inicio de sesion correcto");

      let nombre_apellido = Nombreyapelldio_ref.value

      let ubicacion = ubicacion_ref.value

      let correo = emailregitrar_ref.value

      //Este es utlizado para guardar los usuarios en el database//

      set(ref(database, 'usuarios/'), {

        Nombreapellido: nombre_apellido,
        ubicacion: ubicacion,
        correo: correo

      })

    })

    //Esta promesa se activa si el usuario no se registro correctamente//

    .catch((error) => {
      const errorCode = error.code;

      const errorMessage = error.message;

      console.log("Codigo de error: " + errorCode + "mensaje:" + errorMessage);
    });

  console.log("Creacion de usuario finalizada");
}

//Iniciar sesion//
//Esta funcion lo que hace es iniciar sesion en el usuario con el mail y la contraseña//
function Login() {

  //Este condicional indica si uno de los campos esta vacio marque una alerta//
  if ((emailiniciar_ref.value != '') && (passwordiniciar_ref.value != '')) {

    //Este metodo signInWithEmailAndPassword inicia sesion en el usuario, devolviendo una promesa//
    //Then y Catch son funciones flechas//

    signInWithEmailAndPassword(auth, emailiniciar_ref.value, passwordiniciar_ref.value)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        console.log("usuario correcto");
        window.location.href = './chat.html';
        // ...
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log("usuario incorrecto");
        console.log("Codigo de error: " + errorCode + "mensaje:" + errorMessage);
      });
  } else {
    alert("Verificar que ambos campos esten completos")
  }
}
